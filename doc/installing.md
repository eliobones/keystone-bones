# Installing bones_keystone

- [Prerequisites](./prerequisites)

## npm

Install into your SASS projects.

```
npm install @elioway/bones_keystone
yarn add @elioway/bones_keystone
```

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/bones_keystone.git
```
