![](https://elioway.gitlab.io/eliobones/elio-keystone-bones-logo.png)

> "Can you get this done by Wednesday?" **Rosalind Codrington**

# bones_keystone

Starter pack for an elioThing app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [bones_keystone Documentation](https://elioway.gitlab.io/elioway/bones_keystone/)

## Installing

- [Installing bones_keystone](https://elioway.gitlab.io/elioway/bones_keystone/installing.html)

## Requirements

- [elioFaithful Prerequisites](https://elioway.gitlab.io/elioway/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Seeing is Believing

```
You're seeing it.
```

## Nutshell

- [bones_keystone Quickstart](https://elioway.gitlab.io/elioway/bones_keystone/quickstart.html)

## License

[MIT](license)

![](apple-touch-icon.png)
